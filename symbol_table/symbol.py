class Symbol:
    VAR = 1
    FUNCTION = 2

    def __init__(self, type=VAR):
        self.slot = 0
        self.nom = ""
        self.type = type
        self.nbParam = 0

    def __str__(self):
        return "Synbole : slot = %i / nom = %s " % (
            self.slot, self.nom)
