from .symbol import Symbol
from gestionnaire_exception import SemantiqueException


class SymbolTable:
    def __init__(self):
        # Initialiser une pile vide (libre)
        self.table = [{}]

    def debut_bloc(self):
        bloc = {}
        self.table.append(bloc)
        return

    def fin_bloc(self):
        # if len(self.table) == 1:
        #    raise Exception("Can't pop the global table")
        self.table.pop()
        return

    # TODO : nouveauSymbole et recherche faux, à corriger
    # token = entity venant de lexical_token.py
    def nouveau_symbol(self, noeud, type):
        bloc_courant = self.get_courant_table()
        if noeud.nom in bloc_courant:
            if type == Symbol.VAR:
                raise SemantiqueException("L'identifiant %s existe déjà" % noeud.token.identifiant, noeud.token)
            else:
                raise SemantiqueException("La fonction %s existe déjà" % noeud.token.identifiant, noeud.token)
        else:
            symbol = Symbol()
            symbol.nom = noeud.nom
            symbol.type = type
            symbol.nbParam = len(noeud.identifiants)
            bloc_courant[noeud.nom] = symbol
            return symbol

    def recherche(self, noeud, type):
        for bloc in reversed(self.table):
            if noeud.nom in bloc:
                if type == bloc[noeud.nom].type:
                    if type == Symbol.FUNCTION and len(noeud.enfants) != bloc[noeud.nom].nbParam:
                        print(noeud.identifiants)
                        raise SemantiqueException(
                            "La fonction %s n'a pas le bon nombre d'argument : attendu %i argument(s) et actuellement %i"
                            % (noeud.token.identifiant, bloc[noeud.nom].nbParam, len(noeud.enfants)), noeud.token)
                    return bloc[noeud.nom]
        else:
            if type == Symbol.VAR:
                raise SemantiqueException("La variable %s n'a pas été déclaré auparavant " % noeud.token.identifiant,
                                          noeud.token)
            else:
                raise SemantiqueException("La fonction %s n'a pas été déclaré auparavant " % noeud.token.identifiant,
                                          noeud.token)

    def get_table(self):
        return self.table

    def get_courant_table(self):
        return self.table[-1]
