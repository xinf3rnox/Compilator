import os
import subprocess

import main

def test_all_fichiers_prof():
    dossierActuel = os.path.dirname(os.path.realpath(__file__));
    dossierTest = os.path.join(dossierActuel,'tests','pass');
    allFiles = os.listdir(dossierTest);
    for file in allFiles:
        p = os.path.join(dossierTest, file);
        runFileWithoutMain(p,True);
    dossierTest = os.path.join(dossierActuel, 'tests', 'fail');
    allFiles = os.listdir(dossierTest);
    for file in allFiles:
        p = os.path.join(dossierTest, file);
        runFileWithoutMain(p, False);

def test_all_fichiers_perso():
    dossierActuel = os.path.dirname(os.path.realpath(__file__));
    # Changer le pass en fail pour tester nos tests erreurs.
    dossierTest = os.path.join(dossierActuel,'tests','perso','pass');
    allFiles = os.listdir(dossierTest);
    for file in allFiles:
        p = os.path.join(dossierTest, file);
        runFile(p);

def runFile(path):
    print('Fichier : %s' % path);
    file = open(path,'r');
    lignes = file.readlines();
    # Changer msm.exe par msm si vous êtes sous Linux.
    execMsm = os.path.abspath(__file__ + "/../../msm/msm.exe")
    main.main(lignes, True)
    if os.path.exists("temp.txt"):
        fichierInputMsm = open("temp.txt",'r')
        process = subprocess.Popen(execMsm, stdin=fichierInputMsm, stdout=subprocess.PIPE)
        process.wait()
        outs = process.stdout.readline()
        str_output = outs.decode("utf-8").rstrip('\r\n')
        print(str_output)
        fichierInputMsm.close()
        os.remove("temp.txt")

def runFileWithoutMain(path,compare):
    print('Fichier : %s' % path);
    file = open(path,'r');
    file_name = os.path.basename(path)
    file_name = os.path.splitext(file_name)[0]
    lignes = file.readlines();
    lignes.insert(0, 'void main(){');
    lignes.append('}');
    # Changer msm.exe par msm si vous êtes sous Linux.
    execMsm = os.path.abspath(__file__ + "/../../msm/msm.exe")
    main.main(lignes, True)
    if os.path.exists("temp.txt"):
        fichierInputMsm = open("temp.txt","r")
        process = subprocess.Popen(execMsm,stdin=fichierInputMsm,stdout=subprocess.PIPE)
        process.wait()
        outs = process.stdout.readline()
        str_output=outs.decode("utf-8").rstrip('\r\n')
        print(str_output)
        fichierInputMsm.close()
        os.remove("temp.txt")
        if compare:
            # Test de notre output avec le fichier du prof
            file_out = file_name+".out"
            dossierActuel = os.path.dirname(os.path.realpath(__file__))
            dossierTest = os.path.join(dossierActuel, 'tests','out')
            file_out = os.path.join(dossierTest,file_out)
            file_out = open(file_out, 'r')
            file_out_content = file_out.readline()
            file_out_content = ''.join(file_out_content)
            assert file_out_content == str_output


if __name__ == '__main__':
    # changer par test_all_fichiers_prof() si on test les fichiers du prof
    # ne pas oublié de changer le out.c en out.i dans le gen_code.py
    test_all_fichiers_prof()
