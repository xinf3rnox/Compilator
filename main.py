# ici le main .py

import fileinput
import os
import sys
from gen_code import GenCode
from lexical_analyse import LexicalAnalyse
from semantic_analyse import SemanticAnalyse
from syntaxe_analyse import SyntaxeAnalyse
from gestionnaire_exception import CompilException
from gestionnaire_exception import AnalyseException


def main(lines=[],outputFile=False):
    try:
        lexical_analyse = LexicalAnalyse()

        gen_code = GenCode()
        tokens = []

        i = 0
        dossierActuel = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(dossierActuel, 'runtime.txt')) as f:
            for line in f:
                i += 1
                tokens.extend(lexical_analyse.transform_text_to_token(i,line))

        i = 0
        if len(lines)!=0:
            for line in lines:
                i += 1
                tokens.extend(lexical_analyse.transform_text_to_token(i, line))
        else:
            for line in fileinput.input():
                i += 1
                tokens.extend(lexical_analyse.transform_text_to_token(i, line))

        syntaxe_analyse = SyntaxeAnalyse(tokens)

        noeud = syntaxe_analyse.syntaxe_analyse()
        semantic_analyse = SemanticAnalyse()

        semantic_analyse.analyse_sementique(noeud)
        if outputFile:
            orig_stdout = sys.stdout
            f = open("temp.txt",'w')
            sys.stdout = f
            gen_code.generer_code(noeud)
            sys.stdout = orig_stdout
            f.close()
        else:
            gen_code.generer_code(noeud)

    except CompilException as e:
        exception_analyse = AnalyseException()
        exception_analyse.analyse(e)

    return 0


if __name__ == '__main__':
    main()
