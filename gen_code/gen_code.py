import uuid

from syntaxe_analyse import NoeudType


class GenCode:

    def __init__(self):
        self.labels_pile = []
        self.L1 = None
        self.L2 = None

    def gen_label(self):
        return uuid.uuid4().hex[:6].upper()

    def push(self, L1, L2):
        self.labels_pile.append(L1)
        self.labels_pile.append(L2)

    def pop(self):
        return self.labels_pile.pop(), self.labels_pile.pop()

    def generer_code(self, noeud):
        if noeud.type == NoeudType.noeud_valeur:
            print("push.i", noeud.valeur)
        elif noeud.type == NoeudType.noeud_add:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("add.i")
        elif noeud.type == NoeudType.noeud_moinsB:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("sub.i")
        elif noeud.type == NoeudType.noeud_moinsU:
            self.generer_code(noeud.enfants[0])
            print("push.i -1")
            print("mul.i")
        elif noeud.type == NoeudType.noeud_mul:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("mul.i")
        elif noeud.type == NoeudType.noeud_div:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("div.i")
        elif noeud.type == NoeudType.noeud_mod:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("mod.i")
        elif noeud.type == NoeudType.noeud_equals:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("cmpeq.i")
        elif noeud.type == NoeudType.noeud_not_equals:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("cmpne.i")
        elif noeud.type == NoeudType.noeud_inferieur:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("cmplt.i")
        elif noeud.type == NoeudType.noeud_inferieur_equals:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("cmple.i")
        elif noeud.type == NoeudType.noeud_superieur:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("cmpgt.i")
        elif noeud.type == NoeudType.noeud_superieur_equals:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("cmpge.i")
        elif noeud.type == NoeudType.noeud_and:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("and")
        elif noeud.type == NoeudType.noeud_inverse:
            self.generer_code(noeud.enfants[-1]);
            print("not");
        elif noeud.type == NoeudType.noeud_or:
            self.generer_code(noeud.enfants[0])
            self.generer_code(noeud.enfants[1])
            print("or")
        elif noeud.type == NoeudType.noeud_block:
            for child in noeud.enfants:
                self.generer_code(child)
        elif noeud.type == NoeudType.noeud_aff:
            self.generer_code(noeud.enfants[0])
            print("set {} ; var {}".format(noeud.slot, noeud.nom))
        elif noeud.type == NoeudType.noeud_varRef:
            print("get {} ; var {}".format(noeud.slot, noeud.nom))
        elif noeud.type == NoeudType.noeud_break:
            print("jump", self.L2)
        elif noeud.type == NoeudType.noeud_continue:
            print("jump", self.L1)
        elif noeud.type == NoeudType.noeud_cond:
            L1 = self.gen_label()
            L2 = self.gen_label()
            self.generer_code(noeud.enfants[0])
            print("jumpf", L1)
            self.generer_code(noeud.enfants[1])
            if len(noeud.enfants) == 3:
                print("jump", L2)
            print("." + L1)
            if len(noeud.enfants) == 3:
                self.generer_code(noeud.enfants[2])
                print("." + L2)
        elif noeud.type == NoeudType.noeud_loop:
            self.push(self.L1, self.L2)
            self.L1 = self.gen_label()
            self.L2 = self.gen_label()
            print("." + self.L1)
            self.generer_code(noeud.enfants[0])
            print("jump", self.L1)
            print("." + self.L2)
            self.L2, self.L1 = self.pop()
        elif noeud.type == NoeudType.noeud_function:
            print("." + noeud.nom)
            for nlocal in range(noeud.nlocal):
                print("push.i 42 ; var local")
            self.generer_code(noeud.enfants[0])
            print("push.i 0")
            print("ret")
        elif noeud.type == NoeudType.noeud_return:
            self.generer_code(noeud.enfants[0])
            print("ret")
        elif noeud.type == NoeudType.noeud_out:
            self.generer_code(noeud.enfants[0])
            print("out.c")
        elif noeud.type == NoeudType.noeud_indir:
            print("get {} ; var {}".format(noeud.slot, noeud.nom))
            print("read")
        elif noeud.type == NoeudType.noeud_index:
            print("get {} ; var {}".format(noeud.slot, noeud.nom))
            self.generer_code(noeud.enfants[0])
            print("add.i")
            print("read")
        elif noeud.type == NoeudType.noeud_indir_set:
            print("get {} ; var {}".format(noeud.slot, noeud.nom))
            self.generer_code(noeud.enfants[0])
            print("write")
        elif noeud.type == NoeudType.noeud_index_set:
            print("get {} ; var {}".format(noeud.slot, noeud.nom))
            self.generer_code(noeud.enfants[0])
            print("add.i")
            self.generer_code(noeud.enfants[1])
            print("write")
        elif noeud.type == NoeudType.noeud_call:
            print("prep", noeud.nom)
            for child in noeud.enfants:
                self.generer_code(child)
            print("call", len(noeud.enfants))
        elif noeud.type == NoeudType.noeud_program:
            print(".start")
            print("prep init")
            print("call 0")
            print("prep main")
            print("call 0")
            # print("out.c")
            print("halt")
            for child in noeud.enfants:
                self.generer_code(child)
        else:
            return None
