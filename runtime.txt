void printValeur(int a){
    if(a==0){
        out(48);
    }else{
        if((a/10)>0){
            printValeur(a/10);
        }
        out(48+a%10);
    }
}

void printi(int a){
    printValeur(a);
    out(13);
    out(10);
}

int addblock(int list, int adresse){
    int next;
    int prec;
    prec = list;
    next = *list;

    if(next == 0){
        *prec = adresse;
        adresse[1] = 0;
        return 0;
    }

    if(next > adresse){
        adresse[1] = *prec;
        *prec = adresse;
        return 0;
    }

    while((next < adresse) && (next != 0)){
        prec = next;
        next = next[1];
    }
    if(prec < adresse){
        int suivant;
        suivant = prec;
        adresse[1] = suivant[1];
        prec[1] = adresse;
    }
}

int removeblock(int list, int adresse){
    int next;
    int prec;
    prec = list;
    next = *list;

    if(next == adresse){
        *prec = adresse[1];
        adresse[1] = 0;
        return 0;
    }

    while((next < adresse) && (next != 0)){
        prec = next;
        next = next[1];
    }
    if(prec < adresse){
        int suivant;
        suivant = prec[1];
        prec[1] = suivant[1];
    }
}

int splitblock(int adresseblock, int taille){
    int tmp;
    int tmp2;
    tmp2 = adresseblock[1];
    tmp = *adresseblock;
    adresseblock[1] = adresseblock+taille;

    *adresseblock = taille;
    adresseblock = adresseblock+taille;
    *adresseblock = tmp-taille;
    adresseblock[1] = tmp2;
    return adresseblock;
}

void fusion(int list){
    int next;
    int prec;
    prec = *list;
    next = prec[1];

    while(next != 0){
        if((*prec + prec) == next){
            *prec = *prec + *next;
            prec[1] = next[1];
            next = prec[1];
        } else {
            prec = next;
            next = next[1];
        }
    }
}

int alloc(int taille){
    int dl;
    int fl;
    int tailleblockfree;
    dl = 0;
    dl = *dl;
    fl = dl + 1;
    tailleblockfree = *fl;

    int next;
    next = 10;
    while(next > 0){
        if(*tailleblockfree < taille){
            next = tailleblockfree[1];
            tailleblockfree = next;
        } else {
            if(*tailleblockfree > taille*2){
                splitblock(tailleblockfree,taille+2);
            }
            removeblock(fl,tailleblockfree);
            addblock(dl,tailleblockfree);
            return tailleblockfree+2;
        }
    }
}

void free(int adresse){
    int dl;
    int fl;
    dl = 0;
    dl = *dl;
    fl = dl + 1;
    removeblock(dl,adresse-2);
    addblock(fl,adresse-2);
    fusion(fl);
}

void init(){
    int dl;
    int fl;
    int first;
    dl = 0;
    dl = *dl;
    fl = dl + 1;
    *fl = dl + 2;
    first = *fl;
    *first = 64512 - *fl;
}