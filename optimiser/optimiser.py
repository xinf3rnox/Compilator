from syntaxe_analyse import NoeudType


class Optimiser:
    def opti_moinU(self, noeud):
        for enfant in noeud.enfants:
            enfant = self.opti_moinU(enfant)
        if noeud.type == NoeudType.noeud_moinsU:
            newNoeud = noeud.enfants[0]
            if newNoeud.type == NoeudType.noeud_valeur:
                newNoeud.valeur = -newNoeud.valeur
                return newNoeud
            elif newNoeud.type == NoeudType.noeud_moinsB:
                newNoeud.enfants[0] = -newNoeud.enfants[0]
                return newNoeud
            elif newNoeud.type == NoeudType.noeud_add:
                newNoeud.enfants[0] = -newNoeud.enfants[0]
                newNoeud.enfants[1] = -newNoeud.enfants[1]
                return newNoeud
        return noeud

    def opti_add(self,noeud):
        for enfant in noeud.enfants:
            enfant = self.opti_add(enfant)
        if noeud.type == NoeudType.noeud_add:
            noeud.type = NoeudType.noeud_valeur
            noeud.valeur = noeud.enfants[0] + noeud.enfants[1]
            noeud.enfants = []
            noeud.nom = ""
        return noeud