from symbol_table import Symbol
from symbol_table import SymbolTable
from syntaxe_analyse import NoeudType


class SemanticAnalyse:
    # L'analyse est initialisé une seule fois
    def __init__(self):
        self.symbolTable = SymbolTable()
        self.nb_var = 0

    def analyse_sementique(self, noeud):
        if noeud.type == NoeudType.noeud_call:
            symbol = self.symbolTable.recherche(noeud, Symbol.FUNCTION)

        if noeud.type == NoeudType.noeud_varDecl:
            symbol = self.symbolTable.nouveau_symbol(noeud, Symbol.VAR)
            symbol.slot = self.nb_var
            noeud.slot = symbol.slot
            self.nb_var += 1
        if noeud.type == NoeudType.noeud_varRef or noeud.type == NoeudType.noeud_aff \
                or noeud.type == NoeudType.noeud_index or noeud.type == NoeudType.noeud_index_set \
                or noeud.type == NoeudType.noeud_indir or noeud.type == NoeudType.noeud_indir_set:
            symbol = self.symbolTable.recherche(noeud, Symbol.VAR)
            noeud.slot = symbol.slot
        if noeud.type == NoeudType.noeud_function:
            self.symbolTable.nouveau_symbol(noeud, Symbol.FUNCTION)
            self.symbolTable.debut_bloc()
            # nb_var_old = self.nb_var
            self.nb_var = 0
            for param in noeud.identifiants:
                s = self.symbolTable.nouveau_symbol(param, Symbol.VAR)
                s.slot = self.nb_var
                self.nb_var += 1
            nb_par = self.nb_var
            for child in noeud.enfants:
                self.analyse_sementique(child)
            self.symbolTable.fin_bloc()
            noeud.nlocal = self.nb_var - nb_par
            # self.nb_var = nb_var_old
        elif noeud.type == NoeudType.noeud_block:
            self.symbolTable.debut_bloc()
            for child in noeud.enfants:
                self.analyse_sementique(child)
            self.symbolTable.fin_bloc()
        else:
            for child in noeud.enfants:
                self.analyse_sementique(child)
        return

    def get_nb_var(self):
        return self.nb_var
