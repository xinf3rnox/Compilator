class Noeud:
    ID_GENERATOR = 0

    def __init__(self, type=None, valeur=0, nom="", enfants=None, nlocal=0, token=None, type_retour=None):
        self.id = Noeud.ID_GENERATOR
        self.type = type
        if enfants is None:
            self.enfants = []
        else:
            self.enfants = enfants
        self.identifiants = []
        self.valeur = valeur
        self.nom = nom
        self.slot = 0
        self.nlocal = nlocal
        self.token = token
        self.type_retour = type_retour
        Noeud.ID_GENERATOR += 1

    def __str__(self):
        str_enfant = ""
        for noeud in self.enfants:
            str_enfant += str(noeud.id) + ","
        return "Noeud : ID = %s / Type = %s / Enfants = {%s} / Valeur = %s / nom = %s  / slot = %i /  identifiants = {%s} \n" % (
            self.id, self.type, str_enfant, self.valeur, self.nom, self.slot, self.identifiants) + self.str_enfants()

    def str_enfants(self):
        string = ""
        for noeud in self.enfants:
            string += str(noeud)
        return string
