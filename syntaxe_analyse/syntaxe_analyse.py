from gestionnaire_exception import SyntaxeException
from lexical_analyse import Categorie
from lexical_analyse import LexicalToken
from syntaxe_analyse import Noeud
from syntaxe_analyse import NoeudType


class SyntaxeAnalyse:
    PRIORITY = "priority"
    BASE = "base"

    def __init__(self, tokens):
        self.tokens = tokens
        self.priority_f = {SyntaxeAnalyse.BASE: True,
                           Categorie.token_etoile: NoeudType.noeud_mul,
                           Categorie.token_slash: NoeudType.noeud_div,
                           Categorie.token_pourcentage: NoeudType.noeud_mod}
        self.priority_t = {SyntaxeAnalyse.BASE: False, SyntaxeAnalyse.PRIORITY: self.priority_f,
                           Categorie.token_moins: NoeudType.noeud_moinsB,
                           Categorie.token_plus: NoeudType.noeud_add}
        self.priority_c = {SyntaxeAnalyse.BASE: False, SyntaxeAnalyse.PRIORITY: self.priority_t,
                           Categorie.token_double_egal: NoeudType.noeud_equals,
                           Categorie.token_different: NoeudType.noeud_not_equals,
                           Categorie.token_inferieur: NoeudType.noeud_inferieur,
                           Categorie.token_inferieur_egal: NoeudType.noeud_inferieur_equals,
                           Categorie.token_superieur: NoeudType.noeud_superieur,
                           Categorie.token_superieur_egal: NoeudType.noeud_superieur_equals}
        self.priority_l = {SyntaxeAnalyse.BASE: False, SyntaxeAnalyse.PRIORITY: self.priority_c,
                           Categorie.token_and: NoeudType.noeud_and}
        self.priority_e = {SyntaxeAnalyse.BASE: False, SyntaxeAnalyse.PRIORITY: self.priority_l,
                           Categorie.token_or: NoeudType.noeud_or}

        self.axiome = self.program
        self.current_token = None
        self.has_return = False

    def syntaxe_analyse(self):
        return self.axiome(self.token_next())

    def p(self, token):
        if token.categorie == Categorie.token_etoile:
            if self.token_next().categorie == Categorie.token_ident:
                return Noeud(NoeudType.noeud_indir, nom=self.current_token.identifiant, token=self.current_token)
            raise SyntaxeException('on attend un identifiant après un accès mémoire',
                                   self.current_token)
        if token.categorie == Categorie.token_valeur:
            return Noeud(NoeudType.noeud_valeur, valeur=token.valeur)
        if token.categorie == Categorie.token_ident:
            if self.token_next().categorie == Categorie.token_parenthese_ouvrante:
                noeud_call = Noeud(NoeudType.noeud_call, nom=token.identifiant, token=token)
                noeud_suivant = self.token_to_noeud(self.token_next(), self.priority_e)
                if noeud_suivant is not None:
                    noeud_call.enfants.append(noeud_suivant)
                    while self.token_next().categorie == Categorie.token_virgule:
                        noeud_suivant = self.token_to_noeud(self.token_next(), self.priority_e)
                        if noeud_suivant is None:
                            raise SyntaxeException('on attend que la fonction prenne en param une expression',
                                                   self.current_token)
                        noeud_call.enfants.append(noeud_suivant)
                if self.current_token.categorie == Categorie.token_parenthese_fermante:
                    return noeud_call
                raise SyntaxeException('on attend une parenthese fermante', self.current_token)
            if self.current_token.categorie == Categorie.token_crochet_ouvrant:
                noeud_suivant = self.token_to_noeud(self.token_next(), self.priority_e)
                if noeud_suivant is None:
                    raise SyntaxeException('on attend une expression', self.current_token)
                if self.token_next().categorie == Categorie.token_crochet_fermant:
                    return Noeud(NoeudType.noeud_index, nom=token.identifiant, token=token, enfants=[noeud_suivant])
                raise SyntaxeException('on attend un crochet fermant', self.current_token)
            self.token_put(self.current_token)
            return Noeud(NoeudType.noeud_varRef, nom=token.identifiant, token=token)
        if token.categorie == Categorie.token_moins:
            noeud_suivant = self.p(self.token_next())
            if noeud_suivant is None:
                raise SyntaxeException('après un moins unaire il faut une variable ou une valeur', self.current_token)
            return Noeud(NoeudType.noeud_moinsU, enfants=[noeud_suivant])
        if token.categorie == Categorie.token_exclamation:
            noeud_suivant = self.p(self.token_next())
            if noeud_suivant is None:
                raise SyntaxeException('après un point d\'exclamation il faut une variable ou une valeur',
                                       self.current_token)
            return Noeud(NoeudType.noeud_inverse, enfants=[noeud_suivant])
        if token.categorie == Categorie.token_parenthese_ouvrante:
            noeud_suivant = self.token_to_noeud(self.token_next(), self.priority_e)
            if self.token_next().categorie is Categorie.token_parenthese_fermante:
                return noeud_suivant
            raise SyntaxeException('Il manque surement une parenthese fermante', self.current_token)
        return None

    def token_to_noeud(self, token, priority):
        if priority[SyntaxeAnalyse.BASE]:
            noeud = self.p(token)
        else:
            noeud = self.token_to_noeud(token, priority[SyntaxeAnalyse.PRIORITY])
        if noeud is None:
            return None
        next = self.token_next()
        if next is not None and next.categorie in priority:
            noeud2 = self.token_to_noeud(self.token_next(), priority)
            return Noeud(priority[next.categorie], enfants=[noeud, noeud2])
        self.token_put(next)
        self.current_token = token
        return noeud

    def affectation(self, token):
        if token.categorie == Categorie.token_etoile:
            ident = self.token_next()
            if ident.categorie == Categorie.token_ident:
                token_next = self.token_next()
                if token_next.categorie == Categorie.token_egal:
                    noeud_suivant = self.token_to_noeud(self.token_next(), self.priority_e)
                    if noeud_suivant is not None:
                        return Noeud(NoeudType.noeud_indir_set, enfants=[noeud_suivant], nom=ident.identifiant,
                                     token=ident)
                    raise SyntaxeException('Il manque une expression après le egal', self.current_token)
            raise SyntaxeException('Il manque un identifiant après le *', self.current_token)
        if token.categorie == Categorie.token_ident:
            token_next = self.token_next()
            if token_next.categorie == Categorie.token_crochet_ouvrant:
                noeud_e0 = self.token_to_noeud(self.token_next(), self.priority_e)
                if noeud_e0 is None:
                    raise SyntaxeException('on attend une expression', self.current_token)
                if self.token_next().categorie == Categorie.token_crochet_fermant:
                    if self.token_next().categorie == Categorie.token_egal:
                        noeud_e1 = self.token_to_noeud(self.token_next(), self.priority_e)
                        if noeud_e1 is not None:
                            return Noeud(NoeudType.noeud_index_set, enfants=[noeud_e0, noeud_e1], nom=token.identifiant,
                                         token=token)
                        raise SyntaxeException('Il manque une expression après le egal', self.current_token)
                raise SyntaxeException('on attend un crochet fermant', self.current_token)
            if token_next.categorie == Categorie.token_egal:
                noeud_suivant = self.token_to_noeud(self.token_next(), self.priority_e)
                if noeud_suivant is not None:
                    return Noeud(NoeudType.noeud_aff, enfants=[noeud_suivant], nom=token.identifiant, token=token)
                raise SyntaxeException('Il manque une expression après le egal', self.current_token)
            else:
                self.token_put(token_next)
                self.current_token = token
        return None

    def accolade(self, token, boucle=False):
        if token.categorie == Categorie.token_accolade_ouvrante:
            token = self.token_next()
            noeud = Noeud(NoeudType.noeud_block)
            while token.categorie != Categorie.token_accolade_fermante:
                noeud_suivant = self.statement(token, boucle)
                if noeud_suivant is not None:
                    noeud.enfants.append(noeud_suivant)
                token = self.token_next()
            return noeud
        return None

    def statement(self, token, boucle=False):
        noeud = self.accolade(token, boucle)
        if noeud is not None:
            return noeud

        noeud_suivant = self.affectation(token)
        if noeud_suivant is not None:
            prec = self.current_token
            if self.token_next().categorie == Categorie.token_point_virgule:
                return noeud_suivant
            raise SyntaxeException('Il manque le point virgule de fin ;', prec)

        noeud_suivant = self.token_to_noeud(token, self.priority_e)
        if noeud_suivant is not None:
            if self.token_next().categorie == Categorie.token_point_virgule:
                return noeud_suivant
            raise SyntaxeException('Il manque le point virgule de fin ;', self.current_token)

        if token.categorie == Categorie.token_if:
            noeud_cond = Noeud(NoeudType.noeud_cond)
            if self.token_next().categorie == Categorie.token_parenthese_ouvrante:
                noeud_suivant = self.token_to_noeud(self.token_next(), self.priority_e)
                if noeud_suivant is None:
                    raise SyntaxeException('Il manque une expression pour le if', self.current_token)
                noeud_cond.enfants.append(noeud_suivant)
                if self.token_next().categorie == Categorie.token_parenthese_fermante:
                    noeud = self.statement(self.token_next(), boucle)
                    noeud_cond.enfants.append(noeud)
                    next = self.token_next()
                    if next.categorie == Categorie.token_else:
                        noeud = self.statement(self.token_next(), boucle)
                        noeud_cond.enfants.append(noeud)
                    else:
                        self.token_put(next)
                    return noeud_cond
                raise SyntaxeException('Il manque une parenthese fermante', self.current_token)
            raise SyntaxeException('Il manque une parenthese ouvrante', self.current_token)

        if token.categorie == Categorie.token_int:
            token_ident = self.token_next()
            if token_ident.categorie == Categorie.token_ident:
                if self.token_next().categorie == Categorie.token_point_virgule:
                    return Noeud(NoeudType.noeud_varDecl, nom=token_ident.identifiant, token=token_ident)
                raise SyntaxeException('Il manque le point virgule de fin int ex : i = 0 ;', token_ident)
            raise SyntaxeException('Il manque l\'identifiant après le int', self.current_token)

        if token.categorie == Categorie.token_while:
            if self.token_next().categorie == Categorie.token_parenthese_ouvrante:
                noeud_e = self.token_to_noeud(self.token_next(), self.priority_e)
                if noeud_e is None:
                    raise SyntaxeException('Il manque une expression pour le while', self.current_token)
                if self.token_next().categorie == Categorie.token_parenthese_fermante:
                    noeud_s = self.statement(self.token_next(), True)
                    if noeud_s is None:
                        raise SyntaxeException('Il manque le statement de la boucle while', self.current_token)
                    return Noeud(NoeudType.noeud_loop, enfants=[
                        Noeud(NoeudType.noeud_cond, enfants=[noeud_e, noeud_s, Noeud(NoeudType.noeud_break)])])
                raise SyntaxeException('il manque une parenthese fermante', self.current_token)
            raise SyntaxeException('il manque une parenthese ouvrante', self.current_token)

        if token.categorie == Categorie.token_do:
            noeud_s = self.statement(self.token_next(), True)
            if noeud_s is None:
                raise SyntaxeException('Il manque le statement de la boucle do while', self.current_token)
            if self.token_next().categorie == Categorie.token_while:
                if self.token_next().categorie == Categorie.token_parenthese_ouvrante:
                    noeud_e = self.token_to_noeud(self.token_next(), self.priority_e)
                    if noeud_e is None:
                        raise SyntaxeException('Il manque une expression pour le while', self.current_token)
                    if self.token_next().categorie == Categorie.token_parenthese_fermante:
                        noeud_cond = Noeud(NoeudType.noeud_cond, enfants=[noeud_e, Noeud(NoeudType.noeud_continue),
                                                                          Noeud(NoeudType.noeud_break)])
                        noeud_block = Noeud(NoeudType.noeud_block, enfants=[noeud_s, noeud_cond])
                        return Noeud(NoeudType.noeud_loop, enfants=[noeud_block])
                    raise SyntaxeException('il manque une parenthese fermante', self.current_token)
                raise SyntaxeException('il manque une parenthese ouvrante', self.current_token)
            raise SyntaxeException('il manque le token while', self.current_token)

        if token.categorie == Categorie.token_for:
            if self.token_next().categorie == Categorie.token_parenthese_ouvrante:
                noeud_a1 = self.affectation(self.token_next())
                if noeud_a1 is None:
                    raise SyntaxeException('On attend ici une affectation', self.current_token)
                if self.token_next().categorie != Categorie.token_point_virgule:
                    raise SyntaxeException('Il devrait y avoir un point virgule', self.current_token)
                noeud_e = self.token_to_noeud(self.token_next(), self.priority_e)
                if self.token_next().categorie != Categorie.token_point_virgule:
                    raise SyntaxeException('Il devrait y avoir un point virgule', self.current_token)
                noeud_a2 = self.affectation(self.token_next())
                if noeud_a2 is None:
                    raise SyntaxeException('On attend ici une affectation', self.current_token)
                if self.token_next().categorie == Categorie.token_parenthese_fermante:
                    noeud_s = self.statement(self.token_next(), True)
                    if noeud_s is None:
                        raise SyntaxeException('On attend ici des accolades et un statement', self.current_token)
                    noeud_block = Noeud(NoeudType.noeud_block, enfants=[noeud_s, noeud_a2])
                    noeud_cond = Noeud(NoeudType.noeud_cond,
                                       enfants=[noeud_e, noeud_block, Noeud(NoeudType.noeud_break)])
                    noeud_loop = Noeud(NoeudType.noeud_loop, enfants=[noeud_cond])
                    return Noeud(NoeudType.noeud_block, enfants=[noeud_a1, noeud_loop])
                raise SyntaxeException('Il devrait y avoir une parenthese fermante', self.current_token)
            raise SyntaxeException('Il devrait y avoir une parenthese ouvrante', self.current_token)

        if token.categorie == Categorie.token_return:
            self.has_return = True
            noeud_e = self.token_to_noeud(self.token_next(), self.priority_e)
            if noeud_e is None:
                raise SyntaxeException('On doit retourner une expression', self.current_token)
            if self.token_next().categorie == Categorie.token_point_virgule:
                return Noeud(NoeudType.noeud_return, enfants=[noeud_e])
            raise SyntaxeException('Il manque un point virgule', self.current_token)

        if token.categorie == Categorie.token_break:
            if not boucle:
                raise SyntaxeException('Le token break doit être dans une boucle', token)
            if self.token_next().categorie == Categorie.token_point_virgule:
                return Noeud(NoeudType.noeud_break)
            raise SyntaxeException('Il manque le point virgule', self.current_token)
        if token.categorie == Categorie.token_continue:
            if not boucle:
                raise SyntaxeException('Le token continue doit être dans une boucle', token)
            if self.token_next().categorie == Categorie.token_point_virgule:
                return Noeud(NoeudType.noeud_continue)
            raise SyntaxeException('Il manque le point virgule', self.current_token)

        if token.categorie == Categorie.token_out:
            noeud_e = self.token_to_noeud(self.token_next(), self.priority_e)
            if noeud_e is None:
                raise SyntaxeException('On doit avoir une expression a print', self.current_token)
            if self.token_next().categorie == Categorie.token_point_virgule:
                return Noeud(NoeudType.noeud_out, enfants=[noeud_e])
            raise SyntaxeException('Il manque un point virgule', self.current_token)

        raise SyntaxeException('Ce token n\'est pas un debut de statement valide', token)

    def definition_function(self, token):
        if token.categorie == Categorie.token_int or token.categorie == Categorie.token_void:
            self.has_return = False
            if self.token_next().categorie == Categorie.token_ident:
                function = Noeud(NoeudType.noeud_function, type_retour=token.categorie,
                                 nom=self.current_token.identifiant, token=self.current_token)
                if self.token_next().categorie == Categorie.token_parenthese_ouvrante:
                    if self.token_next().categorie == Categorie.token_int:
                        self.get_identifiants(function, self.current_token)
                        while self.token_next().categorie == Categorie.token_virgule:
                            self.get_identifiants(function, self.token_next())
                    if self.current_token.categorie != Categorie.token_parenthese_fermante:
                        raise SyntaxeException('On attend parenthese fermante', self.current_token)
                    noeud_s = self.accolade(self.token_next())
                    function.enfants.append(noeud_s)
                    if noeud_s is None:
                        raise SyntaxeException('On attend des accolades et un statement',
                                               token)
                    if token.categorie == Categorie.token_int and not self.has_return:
                        raise SyntaxeException('On attend un return dans une fonction avec un retour int',
                                               token)
                    if token.categorie == Categorie.token_void and self.has_return:
                        raise SyntaxeException('On n\'attend pas de return dans une fonction avec un retour void',
                                               token)
                    return function
                raise SyntaxeException('On attend une parenthese ouvrante', self.current_token)
            raise SyntaxeException('On attend un identifiant de fonction', self.current_token)
        raise SyntaxeException('On attend le type de renvoie de la fonction', self.current_token)

    def program(self, token):
        program = Noeud(NoeudType.noeud_program)
        while self.current_token.categorie != Categorie.token_none:
            program.enfants.append(self.definition_function(self.current_token))
            self.token_next()
        return program

    def get_identifiants(self, noeud, token):
        if token.categorie != Categorie.token_int:
            raise SyntaxeException(
                'On attend un type de variable', self.current_token)
        if self.token_next().categorie != Categorie.token_ident:
            raise SyntaxeException('On attend un identifiant', self.current_token)
        noeud.identifiants.append(
            Noeud(NoeudType.noeud_ident, nom=self.current_token.identifiant, token=self.current_token))

    def token_put(self, token):
        self.tokens.insert(0, token)

    def token_next(self):
        self.current_token = self.tokens.pop(0) if len(self.tokens) > 0 else LexicalToken()
        return self.current_token
