from enum import Enum


class NoeudType(Enum):
    noeud_mul = 1
    noeud_div = 2
    noeud_add = 3
    noeud_equals = 4
    noeud_not_equals = 5
    noeud_inferieur = 6
    noeud_inferieur_equals = 7
    noeud_superieur = 8
    noeud_superieur_equals = 9
    noeud_and = 10
    noeud_or = 11
    noeud_aff = 12
    noeud_varDecl = 13
    noeud_varRef = 14
    noeud_block = 15
    noeud_cond = 16
    noeud_loop = 17
    noeud_break = 18
    noeud_moinsU = 19
    noeud_moinsB = 20
    noeud_function = 21
    noeud_return = 22
    noeud_program = 23
    noeud_call = 24
    noeud_inverse = 25
    noeud_continue = 26
    noeud_mod = 27
    noeud_valeur = 28
    noeud_ident = 29
    noeud_out = 30
    noeud_index = 31
    noeud_indir = 32
    noeud_index_set = 33
    noeud_indir_set = 34
