import re

from gestionnaire_exception import SyntaxeException
from .categorie_enum import Categorie
from .lexical_token import LexicalToken


class LexicalAnalyse:
    def __init__(self):
        self.pattern_identifiant = re.compile("")
        self.categorie = {"if": Categorie.token_if,
                          "else": Categorie.token_else,
                          "for": Categorie.token_for,
                          "while": Categorie.token_while,
                          "do": Categorie.token_do,
                          "break": Categorie.token_break,
                          "return": Categorie.token_return,
                          "continue": Categorie.token_continue,
                          "int": Categorie.token_int,
                          "void": Categorie.token_void,
                          "(": Categorie.token_parenthese_ouvrante,
                          ")": Categorie.token_parenthese_fermante,
                          "{": Categorie.token_accolade_ouvrante,
                          "}": Categorie.token_accolade_fermante,
                          "[": Categorie.token_crochet_ouvrant,
                          "]": Categorie.token_crochet_fermant,
                          ";": Categorie.token_point_virgule,
                          "+": Categorie.token_plus,
                          "-": Categorie.token_moins,
                          "*": Categorie.token_etoile,
                          "/": Categorie.token_slash,
                          "%": Categorie.token_pourcentage,
                          "==": Categorie.token_double_egal,
                          "!=": Categorie.token_different,
                          ">": Categorie.token_superieur,
                          "<": Categorie.token_inferieur,
                          ">=": Categorie.token_superieur_egal,
                          "<=": Categorie.token_inferieur_egal,
                          "&&": Categorie.token_and,
                          "||": Categorie.token_or,
                          "!": Categorie.token_exclamation,
                          "&": Categorie.token_adresse,
                          "@": Categorie.token_arobase,
                          "=": Categorie.token_egal,
                          "ident": Categorie.token_ident,
                          "valeur": Categorie.token_valeur,
                          ",": Categorie.token_virgule,
                          "in": Categorie.token_in,
                          "out": Categorie.token_out}

    def transform_text_to_token(self, nb_ligne, line):
        if line.strip().startswith("//"):
            return []
        tokens = []
        chaine = ""
        for i in range(len(line)):
            if line[i] == "/" and i < len(line) + 1 and line[i + 1] == "/":
                return tokens
            if line[i].isspace():
                chaine = ""
                continue
            else:
                chaine += line[i]

            try:
                token = self.transform_chaine_to_token(chaine, line[i + 1] if i < len(line) - 1 else "")
            except SyntaxeException as e:
                token = LexicalToken()
                token.colonne = i
                token.ligne = nb_ligne
                token.texte = line
                e.token = token
                raise e

            if (token != None):
                token.colonne = i
                token.ligne = nb_ligne
                token.texte = line
                tokens.append(token)
                chaine = ""

        return tokens

    def transform_chaine_to_token(self, chaine, next_carac):
        token = LexicalToken()
        if (chaine.isalpha()):
            if (not next_carac.isalnum()):
                if (chaine in self.categorie):
                    token.categorie = self.categorie[chaine]
                    return token
                else:
                    token.categorie = self.categorie["ident"]
                    token.identifiant = chaine
                    return token
        elif (chaine.isdigit()):
            if (not next_carac.isdigit()):
                if (next_carac.isalpha()):
                    raise SyntaxeException("On ne peux pas avoir de token composé de chiffre et ensuite de lettre",
                                           None)
                token.categorie = self.categorie["valeur"]
                token.valeur = chaine
                return token

        elif (chaine.isalnum()):
            if (not next_carac.isalnum()):
                token.categorie = self.categorie["ident"]
                token.identifiant = chaine
                return token
        else:
            if (next_carac == "" or (chaine + next_carac not in self.categorie)):
                if (chaine in self.categorie):
                    token.categorie = self.categorie[chaine]
                    return token
                else:
                    raise SyntaxeException("Ce symbole n'est pas interpreté par notre compilo",
                                           None)
        return None
