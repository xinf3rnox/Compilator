from .categorie_enum import Categorie


class LexicalToken:
    def __init__(self):
        self.categorie = Categorie.token_none
        self.identifiant = ""
        self.valeur = 0
        self.ligne = 0
        self.colonne = 0
        self.texte = ""

    def __str__(self):
        return "Token : Categorie = %s / Identifiant = %s / Valeur = %s / Ligne = %i / Colonne = %i" % (
            self.categorie, self.identifiant, self.valeur, self.ligne, self.colonne)
