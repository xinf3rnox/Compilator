from enum import Enum


class Categorie(Enum):
    token_none = 0
    token_if = 1
    token_else = 2
    token_for = 3
    token_while = 4
    token_do = 5
    token_break = 6
    token_return = 7
    token_continue = 8
    token_int = 9
    token_void = 10
    token_accolade_ouvrante = 11
    token_accolade_fermante = 12
    token_parenthese_ouvrante = 13
    token_parenthese_fermante = 14
    token_crochet_ouvrant = 15
    token_crochet_fermant = 16
    token_point_virgule = 17
    token_plus = 18
    token_moins = 19
    token_etoile = 20
    token_slash = 21
    token_pourcentage = 22
    token_double_egal = 23
    token_different = 24
    token_superieur = 25
    token_inferieur = 26
    token_superieur_egal = 27
    token_inferieur_egal = 28
    token_and = 29
    token_or = 30
    token_not = 31
    token_adresse = 32
    token_arobase = 33
    token_egal = 34
    token_valeur = 35
    token_ident = 36
    token_in = 37
    token_out = 38
    token_virgule = 39
    token_exclamation = 40
