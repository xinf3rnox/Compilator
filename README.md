   ______                      _ __      __
  / ____/___  ____ ___  ____  (_) /___ _/ /_____  _____
 / /   / __ \/ __ `__ \/ __ \/ / / __ `/ __/ __ \/ ___/
/ /___/ /_/ / / / / / / /_/ / / / /_/ / /_/ /_/ / /
\____/\____/_/ /_/ /_/ .___/_/_/\__,_/\__/\____/_/
                    /_/

1 ) Le programme tourne sous Python 3.6.3.

2 ) Pour lancer le programme :

    -> python3 main.py nomdufichier.txt
    -> cat nomdufichier.txt | python3 main.py
    -> python3 main.py < nomdufichier.txt

Les trois façon sont possibles.
