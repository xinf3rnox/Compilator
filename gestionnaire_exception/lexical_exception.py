from .compil_exception import CompilException


class LexicalException(CompilException):

    def __init__(self, message, token):
        # Call the base class constructor with the parameters it needs
        super().__init__(message,token)
        self.type = "Une erreur c'est produite lors de l'analyse lexical \n"
