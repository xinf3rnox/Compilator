from __future__ import print_function
import sys

class AnalyseException:

    def analyse(self,exception):
        var1 = "~" * exception.token.colonne
        var2 = "~" * (50-exception.token.colonne)
        AnalyseException.eprint(exception.token.texte)
        AnalyseException.eprint(var1 + "^" + var2)
        AnalyseException.eprint(exception.type)
        AnalyseException.eprint("Ligne : " + str(exception.token.ligne) + " Colonne : " + str(exception.token.colonne) + " ")
        AnalyseException.eprint(exception)

    def eprint(*args, **kwargs):
        print(*args, file=sys.stderr, **kwargs)