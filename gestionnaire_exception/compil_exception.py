class CompilException(Exception):

    def __init__(self, message, token):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)
        self.token = token
